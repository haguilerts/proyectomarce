
package A2_Objetos;
//import java.text.DateFormat;
//import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
    public class Formulario  {
        // atributos 
         // private DateFormat fechaActual = new SimpleDateFormat("dd/MM/yyyy");
         //private Date fechaActual= new Date();          
          Calendar fecha = Calendar.getInstance();
            int año = fecha.get(Calendar.YEAR);
            int mes = fecha.get(Calendar.MONTH) + 1;
            int dia = fecha.get(Calendar.DAY_OF_MONTH);
            int hora = fecha.get(Calendar.HOUR_OF_DAY);
            int minuto = fecha.get(Calendar.MINUTE);
            int segundo = fecha.get(Calendar.SECOND);
          private String fechaActual=año+"-"+mes+"-"+dia;
          private String horaActual=hora+"h -"+minuto+"m -"+segundo+"s";
          private String numCliente ;
          private String email;
          private String password;         

          private String nombre;
          private String apellido;
          private int dni;
          private String fechaNacieminto; // tipo Date usar atraves de import
          private String direccion;
          private String telefono;
          private String movil;
          private int idmegas;
          

        // cosntructor vacio /cosntructor con parametros
        public Formulario() {  }
        public Formulario(String fechaActual,String horaActual, String numCliente,  String email, String pass, String nombre, String Apellido, int dni, String fechaNacieminto, String direccion, String telefono, String movil,int idmegas) {
          this.fechaActual=fechaActual; this.horaActual=horaActual; this.numCliente =numCliente; this.email = email;  this.password = pass;    this.nombre = nombre; 
          this.apellido = Apellido; this.dni = dni;   this.fechaNacieminto = fechaNacieminto;  this.direccion = direccion; this.telefono = telefono;  this.movil = movil;  this.idmegas= idmegas;
        }
            
        //set

        public String getHoraActual() {
            return horaActual;        } 
        public String getFechaActual() {
            return fechaActual;    }
        public String getNumCliente() {
            return numCliente;    }      
        public String getEmail() {
            return email;  }    
        public String getPassword() {
            return password;   }             
        public String getNombre() {
            return nombre;   }    
        public String getApellido() {
            return apellido;   }    
        public int getDni() {
            return dni;   }    
        public String getFechaNacieminto() {
            return fechaNacieminto;   }    
        public String getDireccion() {
            return direccion;   }    
        public String getTelefono() {
            return telefono;    }    
        public String getMovil() {
            return movil;   }
        public int getIdmegas() {
            return idmegas;    }
        // get  
        
        public void setHoraActual(String horaActual) {
            this.horaActual = horaActual;    }
        public void setFechaActual(String fechaActual) {
            this.fechaActual = fechaActual;    }
        public void setNumCliente(String numCliente) {
            this.numCliente = numCliente;    }
        public void setPassword(String password) {
            this.password = password;   }        
        public void setNombre(String nombre) {
                this.nombre = nombre;   }
        public void setApellido(String apellido) {
                this.apellido = apellido;   }
        public void setDni(int dni) {
                this.dni = dni;   }
        public void setFechaNacieminto(String fechaNacieminto) {
                this.fechaNacieminto = fechaNacieminto;   }
        public void setDireccion(String direccion) {
                this.direccion = direccion;   }
        public void setTelefono(String telefono) {
                this.telefono = telefono;   }
        public void setMovil(String movil) {
            this.movil = movil;   } 
        public void setIdmegas(int idmegas) {
            this.idmegas = idmegas;    }
        public void setEmail(String email) throws Exception  {
          if ( email.contains("@") ) {
            this.email = email;
         } else {
            throw new Exception("Ponele Arroba al Email!!!!");
        } }

        // Tostring

    @Override
    public String toString() {
        return "{" + "fechaActual=" + fechaActual + ", horaActual=" + horaActual + ", numCliente=" + numCliente + ", email=" + email + ", password=" + password + 
                ", nombre=" + nombre + ", apellido=" + apellido + ", dni=" + dni + ", fechaNacieminto=" + fechaNacieminto + 
                ", direccion=" + direccion + ", telefono=" + telefono + ", movil=" + movil + ", idmegas="+idmegas+'}';
    }

        public void validar () throws Exception{
            this.setEmail(this.getEmail());
        }
}