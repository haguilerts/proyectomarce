
package A2_Objetos;

//import java.util.Calendar;

public class Usuario extends Persona{
   private String numUsuario;
   private int montoMensual;
   private String plan;
   private Pagos Pago =new Pagos();

   public Usuario() {} 
   public Usuario(String numUsuario,String nombre, String apellido, int dni, String plan, int montoMensual,  String email, int telefono, String direccion) {
        super(nombre, apellido, dni, email, telefono, direccion);
        this.numUsuario = numUsuario;
        this.plan = plan;
        this.montoMensual = montoMensual;
    }

    public String getNumUsuario() {
        return numUsuario;
    }

    public void setNumUsuario(String numUsuario) {
        this.numUsuario = numUsuario;
    }


    public String getPlan() {
        return plan;
    }

    public void setPlan(String plan) {
        this.plan = plan;
    }

    public int getMontoMensual() {
        return montoMensual;
    }

    public void setMontoMensual(int montoMensual) {
        this.montoMensual = montoMensual;
    }

    public Pagos getMedioPago() {
        return Pago;
    }

    public void setMedioPago(Pagos medioPago) {
        this.Pago = medioPago;
    }

    @Override
    public String toString() {
        return super.toString() +"--- Usuario:{" + "numUsuario=" + numUsuario + ", montoMensual=" + montoMensual + ", plan=" + plan +  ", Pago:" + Pago + '}';
    }

  
   
   
}
