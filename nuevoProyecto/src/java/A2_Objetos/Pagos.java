
package A2_Objetos;
import java.util.Date;
public class Pagos {
    private String medioPago;
    private int numPago;
    private String fechaExp;
    private Date fechaPagado;
    
    public Pagos(){}
    public Pagos(String medioPago, int numPago, String fechaExp, Date fechaPagado) {
        this.medioPago = medioPago;
        this.numPago = numPago;
        this.fechaExp = fechaExp;
        this.fechaPagado = fechaPagado;
    }

    public String getMedioPago() {
        return medioPago;
    }

    public void setMedioPago(String medioPago) {
        this.medioPago = medioPago;
    }

    public int getNumPago() {
        return numPago;
    }

    public void setNumPago(int numPago) {
        this.numPago = numPago;
    }

    public String getFechaExp() {
        return fechaExp;
    }

    public void setFechaExp(String fechaExp) {
        this.fechaExp = fechaExp;
    }

    public Date getFechaPagado() {
        return fechaPagado;
    }

    public void setFechaPagado(Date fechaPagado) {
        this.fechaPagado = fechaPagado;
    }

    @Override
    public String toString() {
        return "---Pagos:{" + "medioPago=" + medioPago + ", numPago=" + numPago + ", fechaExp=" + fechaExp + ", fechaPagado=" + fechaPagado + '}';
    }
     
    
    
}
