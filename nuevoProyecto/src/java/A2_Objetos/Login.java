/** @author haguilerts */
package A2_Objetos;

public class Login {
    // atributos 
      private String mail;
      private String password;  
      
// metodos constructor
    public Login(){}
    public Login(String mail, String password) {
        this.mail = mail;
        this.password = password;
            
        
    }
    
// GET 
    public String getEmail() {
        return mail;   }    
    public String getPassword() {
        return password;    }
    
// SET    
    public void setPassword(String password) {
        this.password = password;    }
    public void setEmail(String mail) throws Exception  {
          if ( mail.contains("@") ) {
            this.mail = mail;
         } else {
            throw new Exception("Ponele Arroba al Email!!!!");
        }}
    
// toString

    @Override
    public String toString() {
        return "{" + "mail=" + mail + ", password=" + password + '}';
    }

    
   
      
      
    
}
