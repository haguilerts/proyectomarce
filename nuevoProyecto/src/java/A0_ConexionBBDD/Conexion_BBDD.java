
package A0_ConexionBBDD;

import java.io.IOException;
//import static java.lang.Class.forName;
import java.sql.*;

public class Conexion_BBDD {
    private static Conexion_BBDD INSTANCE = null;
    private static  String BBDD = "jdbc:mysql://localhost:3306/2_proyectojava";
    private static  String USUARIO = "root"; //"haguilerts";
    private static  String CLAVE = "";//121qseawd1";
    
    // costructor vacio
    private Conexion_BBDD () throws ClassFormatError, IOException, SQLException{
    }
    // instacia la clase 
    public static Conexion_BBDD getInstance() throws ClassFormatError, IOException, SQLException{
        if (INSTANCE==null){
            INSTANCE=new Conexion_BBDD();
        } return INSTANCE; 
    }
    // conexion 
    public Connection getConnection() throws ClassFormatError, IOException, SQLException, ClassNotFoundException{
        //class.forName("com.mysql.jdbc.Driver");
         Class.forName("com.mysql.jdbc.Driver");
        return DriverManager.getConnection( BBDD,USUARIO, CLAVE );
    }
}
