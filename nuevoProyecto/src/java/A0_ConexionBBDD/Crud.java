
package A0_ConexionBBDD;

    import A2_Objetos.Formulario;
    import A2_Objetos.Login;
    import java.sql.*;
    import java.io.IOException;
    import java.sql.SQLException;
    import java.text.DateFormat;
    import java.util.ArrayList;
/*

SELECT c.numCliente, c.mail, c.nombre,c.apellido,c.dni,c.direccion,c.telefono,c.movil,c.fechaActual, 
		 i.megas, i.precio
        FROM clientes c INNER JOIN internet i ON c.idmegas=i.megas
        WHERE c.mail="mario@gmail.com";
*/
public class Crud {
    
        private static String SQL_Formulrio_insertar = "INSERT INTO clientes (mail,password,nombre,apellido,fechaNacimiento,direccion,telefono, movil,fechaActual,horaActual,numCliente,dni,idmegas) value(?,?,?,?,?,?,?,?,?,?,?,?,?);";
        
       
        //"UPDATE clientes SET mail = 'haguilerts@gmail.com' WHERE idcliente = 1";        
            
//////////////////////////////////////////// insertar formulario //////////////////////////////////////////////////////////////////////////////

    public static void insertar(Formulario f) throws ClassNotFoundException, IOException, SQLException {
      //  Formulario f=new Formulario();
        Conexion_BBDD cn= Conexion_BBDD.getInstance();        
                Connection c=cn.getConnection();
                PreparedStatement pst = null ;        
            try{
                c= Conexion_BBDD.getInstance().getConnection();
                pst=c.prepareStatement(SQL_Formulrio_insertar);
                pst.setString(1,f.getEmail());
                pst.setString(2,f.getPassword()); 
                pst.setString(3,f.getNombre()); 
                pst.setString(4,f.getApellido()); 
                pst.setString(5,f.getFechaNacieminto()); 
                pst.setString(6,f.getDireccion()); 
                pst.setString(7,f.getTelefono()); 
                pst.setString(8,f.getMovil()); 
                pst.setString(9,f.getFechaActual());
                pst.setString(10,f.getHoraActual());
                pst.setString(11, f.getNumCliente());
                
                pst.setInt(12,f.getDni());
                pst.setInt(13,f.getIdmegas());
                System.out.println("pst: "+pst);
                 int retorno = pst.executeUpdate();
                 System.out.println("insert:"+retorno);
                if (retorno>0)  System.out.println("Insertado correctamente"); 
            } finally{
                c.close();
                pst.close();
            }
    }
//////////////////////////////////////////// consulta infoCliente //////////////////////////////////////////////////////////////////////////////
    private static String SQL_Formulrio_consultar="SELECT numCliente,mail,password,nombre,apellido,dni,direccion,telefono,movil,fechaActual  FROM clientes  WHERE idcliente >0;";
       
    public static ArrayList<Formulario> consulta () throws SQLException, Exception{
        ArrayList<Formulario> lista = new ArrayList();       
        Conexion_BBDD cn= Conexion_BBDD.getInstance();        
                Connection c=cn.getConnection();
                PreparedStatement pst = null  ; 
                ResultSet rs = null;
                String name = null;
            try {
            pst=c.prepareStatement(SQL_Formulrio_consultar);
            rs=pst.executeQuery();
            Formulario a=null;
            while (rs.next()) {
                try {
                    a=new Formulario();
                    a.setNumCliente(rs.getString("numCliente"));
                    a.setEmail(     rs.getString("mail"));
                    a.setPassword(  rs.getString("password"));
                    a.setNombre(    rs.getString("nombre"));
                    a.setApellido(  rs.getString("apellido"));
                    a.setDni(       rs.getInt("dni"));
                    a.setDireccion( rs.getString("direccion"));
                    a.setTelefono(  rs.getString("telefono"));
                    a.setMovil(     rs.getString("movil"));
                    a.setFechaActual(rs.getString("fechaActual"));
                    /*megas , precio, fecha de pago,.*/
                    
                    //System.out.println("Crud_formulario : "+ a);
//              
                } catch (Exception e) {
                    System.out.println("errorCRUD: "+e);
                }
                lista.add(a);
                //System.out.println("arrayFormulario: "+lista.add(a));
            }
        } finally{
                c.close();
                pst.close();
                
            }
        return lista ;
        //return name;    
    }
////////////////////////////////////////////  consulta login //////////////////////////////////////////////////////////////////////////////
    private static String SQL_Formulrio_login="SELECT mail, password FROM clientes WHERE idcliente > 0 ;";
    
    public static ArrayList<Login> consultaLogin() throws ClassFormatError, IOException, SQLException, ClassNotFoundException, Exception {
         ArrayList<Login> listaLog = new ArrayList(); 
           Conexion_BBDD cn= Conexion_BBDD.getInstance();        
                Connection c=cn.getConnection();
                PreparedStatement pst = null  ; 
                ResultSet rs = null;
                String name = null;
        try {
            pst=c.prepareStatement(SQL_Formulrio_login);
            rs=pst.executeQuery();
            Login u=null;
            while (rs.next()) {
                try {
                    u=new Login();
                    u.setEmail(rs.getString("mail"));
                    u.setPassword(rs.getString("password"));
                    System.out.println("Crud.login: "+ u);
 
                } catch (SQLException e) {
                    System.out.println("ex:"+e);
                }
                listaLog.add(u);
            }
        } finally{
                c.close();
                pst.close();                
            }   
        return listaLog ;
    }
//////////////////////////////////////////// actualizar  infoClientes//////////////////////////////////////////////////////////////////////////////
     private static String SQL_infoCliente_update="UPDATE clientes SET mail=? WHERE numCliente = ?;"; 
    
        public static void actualizar(Formulario a) throws ClassNotFoundException, IOException, SQLException {
            Conexion_BBDD cn= Conexion_BBDD.getInstance();        
                Connection c=cn.getConnection();
                PreparedStatement pst = null  ; 
                ResultSet rs = null;
                String name = null;
            try {
                c = cn.getInstance().getConnection();
                pst = c.prepareStatement(SQL_infoCliente_update);
                pst.setString(1, a.getEmail());
                pst.setInt(2, Integer.parseInt(a.getNumCliente()));
                pst.execute();
            } finally {
                try {
                    pst.close();
                } finally {
                    c.close();
                }
            }
        }

    public static void actualizar(Formulario actObj, String numCliente) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
