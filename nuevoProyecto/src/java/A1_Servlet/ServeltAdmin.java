package A1_Servlet;

import A5_Crud.B_Consulta;
import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.Json;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/** @author haguilerts  */
@WebServlet(name = "ServeltAdmin", urlPatterns = {"/ServeltAdmin"})
public class ServeltAdmin extends HttpServlet {
    Gson convertirJson = new Gson(); // para q no salga error, añadimos la libreria e importamos 

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            System.out.println("invocando[]: "+B_Consulta.consultaAdmin());
        String infJson = convertirJson.toJson(B_Consulta.consultaAdmin());
            
            resp.getWriter().print(infJson);
            System.out.println("infoJason: "+infJson);
            
            
        } catch (ClassFormatError | SQLException | ClassNotFoundException ex) {
            Logger.getLogger(ServeltAdmin.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
    }

  
}
/*
BBDD:

CREATE TABLE persona (
    idPerson int(11) NOT null AUTO_INCREMENT PRIMARY KEY,
    name varchar(50) null,
    surname varchar(50) null,
	dni int(11) not null,
    email varchar(50) null,
    phone int (15) null,
    direction varchar(50) null
)
CREATE TABLE plan (
    idPlan int(11) NOT null AUTO_INCREMENT PRIMARY KEY,
    plan varchar(10) null,
    montoMensual int(50) null
)
CREATE TABLE medioPago (
    idPago int(11) NOT null AUTO_INCREMENT PRIMARY KEY,
    namePago varchar(10) null
)
CREATE TABLE numeroPago (
    idPago int(11) NOT null AUTO_INCREMENT PRIMARY KEY,
    numPago varchar(10) null
)
CREATE TABLE pago (
    idPago int(11) NOT null AUTO_INCREMENT ,
    fechaExp varchar(10) null,
    fechaPagado date null,
	FKmedioPago int(11) NOT null,
    FKnumPago int(11) NOT null,
    PRIMARY KEY(idPago),
	FOREIGN KEY (FKmedioPago) REFERENCES mediopago(idMedioPago),
	FOREIGN KEY (FKnumPago) REFERENCES numeropago(idNumPago)
)
CREATE TABLE usuario (
    idUsuario int(11) NOT null AUTO_INCREMENT ,
    fechaInicio varchar(10) null,
	FKplan int(11) NOT null,
    FKPago int(11) NOT null,
    PRIMARY KEY(idUsuario),
	FOREIGN KEY (FKplan) REFERENCES plan(idPlan),
	FOREIGN KEY (FKPago) REFERENCES pago(idPago)
)
CREATE TABLE cliente (
    idCliente int(11) NOT null AUTO_INCREMENT ,
    numCliente varchar(10) NOT null,
	FKPersona int(11) NOT null,
    FKUsuario int(11) NOT null,
    estado boolean DEFAULT false,
    PRIMARY KEY(idCliente,numCliente),
	FOREIGN KEY (FKPersona) REFERENCES persona(idPerson),
	FOREIGN KEY (FKUsuario) REFERENCES usuario(idUsuario)
)

********************************************************************************
INSERT INTO persona VALUES
(null,"giovanny","aguilar",123456789,"gio@gmail.com",1166015483,"av.xxx 123"),
(null,"rhonny","rojas",987654321,"rhonny@gmail.com",1166332255,"av.zzz 321")
;
INSERT INTO plan VALUES
(null,"1MG",100),
(null,"2MG",200),
(null,"3MG",300),
(null,"4MG",400),
(null,"5MG",500),
(null,"1GB",1000),
(null,"5GB",2000),
(null,"10GB",5000),
(null,"20GB",6000),
(null,"30GB",7000)
;
INSERT INTO mediopago VALUES
(null,"Pago Facil"),
(null,"Rapi Pago"),
(null,"Targeta de Credito")
;
INSERT INTO numeropago VALUES
(null, 994958951),
(null, 963258741),
(null, 987456321),
(null, 147852369),
(null, 123654789),
(null, 789654123),
(null, 321456987),
(null, 369852147),
(null, 654789321)
;
INSERT INTO pago VALUES
(null,"2020-02-10",2020-02-06,1,2 ),
(null,"2020-03-10",2020-03-07,1,3 ),
(null,"2020-04-10",2020-04-08,1,4 ),
(null,"2020-05-10",2020-05-09,1,5 ),
(null,"2020-06-10",2020-06-10,1,6 ),
(null,"2020-07-10",2020-07-11,1,7 )
;
INSERT INTO usuario VALUES
(null,"2020-01-01",1,1 ),
(null,"2020-02-02",2,24 ),
(null,"2020-01-03",3,25 ),
(null,"2020-01-10",4,26 ),
(null,"2020-03-15",5,27 )
;
INSERT INTO cliente VALUES
(null,"01",1,11,0),
(null,"02",2,12,1)
;

*/