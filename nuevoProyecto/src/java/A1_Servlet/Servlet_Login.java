package A1_Servlet;

import A3_Filtros.Filtros;
import A0_ConexionBBDD.Crud;
import A2_Objetos.Formulario;
import A2_Objetos.Login;
import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "Servlet_Login", urlPatterns = {"/Servlet_Login"})
public class Servlet_Login extends HttpServlet {

    Gson convertirJson = new Gson(); // para q no salga error, añadimos la libreria e importamos 
    ArrayList<Formulario> miListado = new ArrayList();
    public Login guardarLogin;

    public Login getGuardarLogin() {
        return guardarLogin;    }
    public void setGuardarLogin(Login guardarLogin) {
        this.guardarLogin = guardarLogin;    }
    
    private final String t = "true";
    private final String f = "falce";

    
    // ************************************ doPost ************************************
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println(" ---- Estas en DoPost login ----");
        String pedidoLogin = req.getReader().readLine();
        // destrasformo el pedido(Json) a lenguaje java y guardo las varible del mismo tipo y nombre q coincidan 
        //de la Formulario.java (class). y todo eso lo guardo dentro una variable "guardado" de tipo Formulario.                
        guardarLogin = convertirJson.fromJson(pedidoLogin, Login.class);
       
        try {     
             Filtros.filtro(guardarLogin);
            if(Filtros.filtro2(guardarLogin)== true){
                resp.getWriter().println(convertirJson.toJson(t));
                System.out.println("true...");
            }else { 
                resp.getWriter().println(convertirJson.toJson(f));
                System.out.println("falce...");
            }
        } catch (Exception ex) {
            System.out.println("errorException: "+ex);
        }
        System.out.println("pedido(Json): " + pedidoLogin);// imprimo el formato "Json" en la consola de netbeans
        System.out.println("guardado(Java): " + guardarLogin);// imprimo el formato "Java" en la consola de netbeans

    }
    // ************************************ doGet ************************************
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println(" ---- Estas en doGet login  if(filtro()==true){ ---");
           // System.out.println("filtro:" + filtro());
       /* if(filtro()== true){
            String infJson = convertirJson.toJson("estas  en doGet ");
            resp.getWriter().print(infJson);
            System.out.println(infJson);
        } else{
        }*/
//             Login objLog=guardarLogin;
//           
//              System.out.println("mail: "+objLog.getEmail());
//              System.out.println("pas: "+objLog.getPassword());
       if(true){
            try {

                 String infJson = convertirJson.toJson(Filtros.filtro(guardarLogin));
                 resp.getWriter().print(infJson);
                 //String infJson = convertirJson.toJson(Crud.consulta()); // envia la info
                 //resp.getWriter().print("resp de doGet");
                 //ok="bienvenido cliente";
                 System.out.println("bienvenido cliente");
                 //System.out.println("inf[mail,pass]: " + Crud.consulta()); // traer el infoCliente
                 System.out.println("infJson: "+infJson);
                 System.out.println(" -------------------------------------------");

             } catch (Exception ex) {
                System.out.println("GetExepcion: "+ex);
             }
       } else{ System.out.println("mal--");}
    }
    
    
    
}
