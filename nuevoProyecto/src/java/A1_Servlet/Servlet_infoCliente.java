
package A1_Servlet;

import A0_ConexionBBDD.Crud;
import A2_Objetos.Formulario;
import java.util.ArrayList;
import java.sql.SQLException;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.google.gson.Gson;



@WebServlet(name = "Servlet_infoCliente", urlPatterns = {"/Servlet_infoCliente"})
public class Servlet_infoCliente extends HttpServlet {
    
final static Gson CONVERTIR = new Gson();
///////////////////////////////////// consultar /////////////////////////////////
    
    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
       response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            Formulario actObj = CONVERTIR.fromJson(request.getReader(), Formulario.class);
            
            Crud.actualizar(actObj);
            
            System.out.println("obj_infoClientes: "+actObj);
            System.out.println("Json_infoClientes: "+request.getReader());
            out.println(CONVERTIR.toJson("Exito"));
        } catch (Exception ex) {
            out.println("Verificar:" + ex.getMessage());
        } finally {
            out.close();
        }
        
    }
    

   
}
