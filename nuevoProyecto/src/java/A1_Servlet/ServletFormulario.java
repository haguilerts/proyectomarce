
package A1_Servlet;
import A0_ConexionBBDD.Crud;
import A2_Objetos.Formulario;
//import A1servlet.Usuario;
import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author HP
 */
@WebServlet(name = "ServletFormulario", urlPatterns = {"/ServletFormulario"})
public class ServletFormulario extends HttpServlet {

         
    Gson convertirJson = new Gson(); // para q no salga error, añadimos la libreria e importamos 
    ArrayList<Formulario> miListado= new ArrayList(); 
    
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws  ServletException, IOException {
        
                System.out.println("-----Estas en el doPOST del server---- ");  // esto aparece en la parte de java.
              // lo tomo los Json y guardo en una variable pedido
                   String pedidoFormulario=req.getReader().readLine(); 
              // destrasformo el pedido(Json) a lenguaje java y guardo las varible del mismo tipo y nombre q coincidan 
              //de la Formulario.java (class). y todo eso lo guardo dentro una variable "guardado" de tipo Formulario.                
              Formulario guardar = convertirJson.fromJson(pedidoFormulario,Formulario.class);
               
        try {       
            guardar.validar();        
            Crud.insertar(guardar);
            resp.getWriter().println("Se Registro con Exito..");
            
        } catch (ClassNotFoundException | SQLException  ex) {
            System.out.println("verificar: " +ex);
            resp.getWriter().println("Nose realizo la CONEXION !!!...");
        } catch (Exception ex) {
            System.out.println("error"+ex);
            resp.getWriter().println("Verifica el CORREO!!!...");
        }
            System.out.println("pedido(Json): "+pedidoFormulario);// imprimo el formato "Json" en la consola de netbeans
            System.out.println("guardado(Java): "+ guardar);// imprimo el formato "Java" en la consola de netbeans

       miListado.add(guardar); 
//            resp.getWriter().println("##### Tu usuario se guardo en nuestra doPost #####");// esto aparece en la web de la parte del servlet.
//            resp.getWriter().println("**Json: " +  pedidoFormulario );
//            resp.getWriter().println("**Jaba: " +   guardar);
//            resp.getWriter().println("**Jaba (nombre): " + guardar.getNombre());
//            resp.getWriter().println("** " + miListado.add(guardar));
       
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        
        System.out.println("-----Estas en el doPOST del server---- ");  // esto aparece en la parte de java.
        
        
    }
             
}
