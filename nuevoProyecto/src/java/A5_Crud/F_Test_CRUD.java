
package A5_Crud;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

public class F_Test_CRUD { 
    public static void insertar(Connection con,PreparedStatement stmt){
        String nombre = "Prima";
        String apellido="rojas perez";
        int dni=54788621;
        String mail = "prima@gmail.com";
        int tel=1166225488;
        String dom = "av.www 123";                     
        try {
            String insert="INSERT INTO `persona` VALUES (null,?,?,?,?,?,?)";
            stmt = con.prepareStatement(insert);
            stmt.setString( 1,nombre);
            stmt.setString( 2,apellido);
            stmt.setInt(    3,dni);
            stmt.setString( 4,mail);
            stmt.setInt(    5,tel);
            stmt.setString( 6,dom);

            int retorno = stmt.executeUpdate();
          if (retorno>0)
             System.out.println("Insertado correctamente"); 
        } catch (SQLException ex) {
            Logger.getLogger(F_Test_CRUD.class.getName()).log(Level.SEVERE, null, ex);
        }      
    }
    
    public static void consultar(Connection con,PreparedStatement stmt){   
        try {
            String consulta="select * from persona";
            // prepara la consulta
            PreparedStatement pstm = con.prepareStatement(consulta);
            
            // realizo la consulta
                ResultSet rs = pstm.executeQuery();
                while (rs.next()) {
                    System.out.println("NOMBRE: " + rs.getString("name") +
                            " APELLIDO: " + rs.getString("surname"));       
                }             
        } catch (SQLException ex) {
            Logger.getLogger(F_Test_CRUD.class.getName()).log(Level.SEVERE, null, ex);
        }      
    }
    
    public static void actualizar(Connection con,Statement stmt){
         try {
            String laActualizacion = "UPDATE persona SET name = 'giovanny' WHERE idPerson = 1;";        
            stmt.execute(laActualizacion); 
            
            System.out.println("MUY BIEN se actualizo con EXITO..");
        } catch (SQLException e) {
            System.out.println("MAL aCTUALIZADO:"+ e);
        }
    }
    
    public static void eliminar(Connection con,Statement stmt){
         try {            
            String delete = "DELETE FROM persona WHERE idPerson = 4";
            stmt.execute(delete);   
            System.out.println("se ELIMINO con EXITO ...");

        } catch (SQLException e) {
            System.out.println("ERRRO de ELIMINACION: "+e);
        }
    }
    
    //**************************************************************************************
    public static void main(String[] args) {
    Connection con = null;
    PreparedStatement stmt = null;

     String sDriver = "com.mysql.jdbc.Driver";
     String sURL = "jdbc:mysql://localhost:3306/2_proyectojava";

     try{
        Class.forName(sDriver).newInstance();    
        con = DriverManager.getConnection(sURL,"haguilerts","121qseawd1");
        Statement st = con.createStatement();

        //C1_Insertar.insertar(con,stmt);       
        //C1_Insertar.consultar(con, stmt);
        //C1_Insertar.actualizar(con, st);
        //C1_Insertar.eliminar(con, st);
     } catch (SQLException sqle){
        System.out.println("SQLState: "  + sqle.getSQLState());
        System.out.println("SQLErrorCode: " + sqle.getErrorCode());
     } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e){
         System.out.println(e);
     } finally {
        if (con != null) {
           try{
              stmt.close();
              con.close();
           } catch(Exception e){
              e.printStackTrace();
           }
        }
     }           
   }
   
    
}
