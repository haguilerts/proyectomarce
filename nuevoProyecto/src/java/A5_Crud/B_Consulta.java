
package A5_Crud;
import A0_ConexionBBDD.Conexion_BBDD;
import A2_Objetos.Usuario;
import A2_Objetos.Pagos;


import java.io.IOException;
import java.sql.*;
import java.sql.Connection;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.util.ArrayList;

import java.util.logging.Level;
import java.util.logging.Logger;



    public class B_Consulta { 
        private static String consulta="SELECT c.numCliente,c.estado,\n" +
                                "p.name,p.surname,p.dni,p.email,p.phone,p.direction,\n" +
                                "u.fechaInicio,\n" +
                                "    pl.plan,pl.montoMensual,\n" +
                                "    pg.fechaExp,pg.fechaPagado,\n" +
                                "		mp.namePago, np.numPago\n" +
                                "FROM cliente c \n" +
                                "JOIN persona p ON c.FKPersona=p.idPerson\n" +
                                "JOIN usuario u ON c.FKUsuario=u.idUsuario\n" +
                                "JOIN plan pl ON u.FKplan=pl.idPlan\n" +
                                "JOIN pago pg ON u.FKPago=pg.idPago\n" +
                                "JOIN mediopago mp ON pg.FKmedioPago=mp.idMedioPago\n" +
                                "join numeropago np ON pg.FKnumPago=np.idNumPago\n" +
                                ";";
        //********************** metodos consulta *************************
        public static ArrayList<Usuario> consultaAdmin() throws ClassFormatError, IOException, SQLException, ClassNotFoundException{
            ArrayList<Usuario> lista = new ArrayList(); 
            
            Conexion_BBDD cn= Conexion_BBDD.getInstance();   // instancio el obj 
            Connection con=cn.getConnection();
            PreparedStatement stmt = null;
            ResultSet rs = null;
                      
            try {                
                stmt = con.prepareStatement(consulta);// prepara la consulta
                rs=stmt.executeQuery();// realizo la consulta
                //System.out.println(rs.getArray(consulta));

                Usuario u=null ;
                Pagos p;
                    while (rs.next()) {

                        try{
                            u=new Usuario();
                                p=new Pagos();
                                p.setNumPago(rs.getInt("numPago"));
                                p.setMedioPago(rs.getString("namePago"));
                                p.setFechaExp(rs.getString("fechaExp"));
                                p.setFechaPagado(rs.getDate("fechaPagado"));                             
                            
                            u.setNumUsuario(rs.getString("numCliente"));
                            u.setNombre(rs.getString("name"));
                            u.setApellido(rs.getString("surname"));
                            u.setDni(rs.getInt("dni"));
                            u.setEmail(rs.getString("email"));
                            u.setDireccion(rs.getString("direction"));
                            u.setTelefono(rs.getInt("phone"));
                            u.setPlan(rs.getString("plan"));
                            u.setMontoMensual(rs.getInt("montoMensual"));
                            u.setMedioPago(p);
                        } catch (SQLException e) {
                            System.out.println("Error:"+e);
                        }
                        lista.add(u);                
                        
                    }            
            } catch (SQLException ex) {
                Logger.getLogger(F_Test_CRUD.class.getName()).log(Level.SEVERE, null, ex);
            }    
            System.out.println("lista: "+lista);
        return lista;
        }

    }
        
        
        
/*ERRORES en la mala coneccion mal ingresado de:

    -direccion de BBDD:  The last packet sent successfully to the server was 0 milliseconds ago.
            The driver has not received any packets from the server = (El último paquete enviado con éxito al servidor
            fue hace 0 milisegundos. El controlador no ha recibido ningún paquete del servidor.)
    -nombre de BBDD:  Unknown database 'formulario*'
    -usuario o password: Access denied for user 'haguilerts1'@'localhost' (using password: YES)


****************************** Teoria: TIPO DE OBJETOS. ******************************
   -----consultas----- 

    Statement: Es el objeto que se encarga de enviar las sentencias SQL(consulta o actualizacion) al driver ( executeQuery or executeUpdate   ).
               Para crear el objeto Statement se debe invocar el método createStatement dentro del objeto del tipo Connection utilizado para
               conectarse a la base de datos. eje: Statement st = con.createStatement();  donde con es un obj del tipo Connection
    ResultSet: El resultado de la consulta es recibido por el API JDBC como un objeto del tipo ResultSet, por ello, el retorno del método 
               executeQuery se asigna a un objeto de éste tipo.

            Ahora que tenemos el resultado se puede trabajar con él. Dentro del objeto ResultSet existen diferentes métodos para extraer la información.
            El primero que se debe conocer es el método next, que permite mover un puntero por las distintas filas que conforman el resultado 
            de la consulta (hay que imagimar en el objeto ResultSet como una tabla). En un principio el puntero se encuentra sobre la primera fila, 
            por ello antes que todo se debe invocar al método next para ingresar a la tabla.
            Para obtener la información se utiliza el método getXXX (donde XXX es el tipo de datos a recuperar),
            por ejemplo getString, getInt, getFloat, etc.

        pagina para tener mas inf:   http://profesores.elo.utfsm.cl/~agv/elo330/2s03/projects/JDBC/jdbc-02.html

   ----- insertar ----- 
    Como insertar datos

El método createStatement()
      El método createStatement() es el mismo presentado en la sección B_Consulta.

El método execute()
      El método execute() se utiliza para ejecutar sentencias SQL del tipo INSERT, UPDATE o DELETE ( no retorna un conjunto de resultados), 
        y a diferencia del método executeQuery() que es para consultas.
     
        https://labojava.blogspot.com/2012/05/jdbc-conexion-con-base-de-datos.html
*/

/*
SELECT c.numCliente,
     p.name,p.surname,p.dni,p.email,p.phone,p.direction,
     u.fechaInicio,
     pl.plan,pl.montoMensual,
     pg.fechaExp,pg.fechaPagado,
     mp.namePago, np.numPago,
     c.estado
     FROM cliente c 
     JOIN persona p ON c.FKPersona=p.idPerson
     JOIN usuario u ON c.FKUsuario=u.idUsuario
     JOIN plan pl ON u.FKplan=pl.idPlan
     JOIN pago pg ON u.FKPago=pg.idPago
     JOIN mediopago mp ON pg.FKmedioPago=mp.idMedioPago
     JOIN numeropago np ON pg.FKnumPago=np.idNumPago
;
*/